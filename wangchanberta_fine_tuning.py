from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from torch.utils.data import DataLoader, RandomSampler, SequentialSampler
from torch.utils.data import TensorDataset
from tqdm import tqdm
import sys, getopt
import pandas as pd
import numpy as np
import random
import torch

from transformers import AutoTokenizer, AutoModelForSequenceClassification
from transformers import AdamW, get_linear_schedule_with_warmup

class fine_tune_wangchanberta():

    def __init__(self):
        self.EPOCHS = 5
        self.BATCH_SIZE = int(sys.argv[1])
        self.MAX_LENGTH = 256
        self.SEED_VAL = 42
        self.MODEL_NAME = "airesearch/wangchanberta-base-att-spm-uncased"
        self.FILE_PATH = 'wisesight.csv'

    def import_data(self):
        df = pd.read_csv(self.FILE_PATH)
        
        self.label_dict = {}
        for index, possible_label in enumerate(df.category.unique()):
            self.label_dict[possible_label] = index

        df['label'] = df.category.replace(self.label_dict)

        x_train, x_val, y_train, y_val =  train_test_split(df.index.values,
                                                        df.label.values,
                                                        test_size=0.10,
                                                        random_state=42,
                                                        stratify=df.label.values
        )

        df['data_type'] = ['not_set']*df.shape[0]
        df.loc[x_train, 'data_type'] = 'train'
        df.loc[x_val, 'data_type'] = 'val'

        return df

    def encode_data(self, df):
        tokenizer = AutoTokenizer.from_pretrained(self.MODEL_NAME)

        # Encoding the Training data
        encoded_data_train = tokenizer.batch_encode_plus(
            df[df.data_type=='train'].texts.values.tolist(), 
            add_special_tokens=True, 
            return_attention_mask=True, 
            pad_to_max_length=True, 
            max_length=self.MAX_LENGTH, 
            return_tensors='pt'
        )

        # Encoding the Validation data
        encoded_data_val = tokenizer.batch_encode_plus(
            df[df.data_type=='val'].texts.values.tolist(), 
            add_special_tokens=True, 
            return_attention_mask=True, 
            pad_to_max_length=True, 
            max_length=self.MAX_LENGTH, 
            return_tensors='pt'
        )

        return encoded_data_train, encoded_data_val

    def data_loader(self, df , encoded_data_train, encoded_data_val):
        input_ids_train = encoded_data_train['input_ids']
        attention_masks_train = encoded_data_train['attention_mask']
        labels_train = torch.tensor(df[df.data_type=='train'].label.values)

        input_ids_val = encoded_data_val['input_ids']
        attention_masks_val = encoded_data_val['attention_mask']
        labels_val = torch.tensor(df[df.data_type=='val'].label.values)

        dataset_train = TensorDataset(input_ids_train, attention_masks_train, labels_train)
        dataset_val = TensorDataset(input_ids_val, attention_masks_val, labels_val)

        self.dataloader_train = DataLoader(dataset_train, 
                                    sampler=RandomSampler(dataset_train),
                                    batch_size=self.BATCH_SIZE)

        self.dataloader_validation = DataLoader(dataset_val, 
                                    sampler=RandomSampler(dataset_val),
                                    batch_size=self.BATCH_SIZE)

    def f1_score_func(preds, labels):

        # Setting up the preds to axis=1
        # Flatting it to a single iterable list of array
        preds_flat = np.argmax(preds, axis=1).flatten()

        # Flattening the labels
        labels_flat = labels.flatten()

        # Returning the f1_score as define by sklearn
        return f1_score(labels_flat, preds_flat, average='weighted')

    def accuracy_per_class(self, preds, labels):
        label_dict_inverse = {v: k for k, v in self.label_dict.items()}
        
        preds_flat = np.argmax(preds, axis=1).flatten()
        labels_flat = labels.flatten()

        # Iterating over all the unique labels
        # label_flat are the --> True labels
        for label in np.unique(labels_flat):
            # Taking out all the pred_flat where the True alable is the lable we care about.
            # e.g. for the label Happy -- we Takes all Prediction for true happy flag
            y_preds = preds_flat[labels_flat==label]
            y_true = labels_flat[labels_flat==label]
            print(f'Class: {label_dict_inverse[label]}')
            print(f'Accuracy: {len(y_preds[y_preds==label])}/{len(y_true)}\n')

    def evaluate(self, model, device):

        model.eval()
        
        loss_val_total = 0
        predictions, true_vals = [], []
        
        for batch in tqdm(self.dataloader_validation):
            
            batch = tuple(b for b in batch)

            inputs = {'input_ids':      batch[0],
                    'attention_mask': batch[1],
                    'labels':         batch[2],
                    }  

            with torch.no_grad():        
                outputs = model(**inputs)
                
            loss = outputs[0]
            logits = outputs[1]
            loss_val_total += loss.item()

            logits = logits.detach().cpu().numpy()
            label_ids = inputs['labels'].cpu().numpy()
            predictions.append(logits)
            true_vals.append(label_ids)
        
        loss_val_avg = loss_val_total/len(self.dataloader_validation) 
        
        predictions = np.concatenate(predictions, axis=0)
        true_vals = np.concatenate(true_vals, axis=0)
                
        return loss_val_avg, predictions, true_vals

    def model_setting(self):
        model = AutoModelForSequenceClassification.from_pretrained(self.MODEL_NAME,
                                                                num_labels=len(self.label_dict),
                                                                output_attentions=False,
                                                                output_hidden_states=False)

        optimizer = AdamW(model.parameters(),
                        lr=1e-5, 
                        eps=1e-8)


        scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                    num_warmup_steps=0,
                                                    num_training_steps=len(self.dataloader_train)*self.EPOCHS)

        random.seed(self.SEED_VAL)
        np.random.seed(self.SEED_VAL)
        torch.manual_seed(self.SEED_VAL)
        torch.cuda.manual_seed_all(self.SEED_VAL)

        device = torch.device('cpu')
        model.to(device)

        return model, optimizer, scheduler, device

    def training(self, model, optimizer, scheduler, device):
        for epoch in tqdm(range(1, self.EPOCHS+1)):
            
            model.train()          # Sending our model in Training mode
            
            loss_train_total = 0   # Setting the training loss to zero initially

            # Setting up the Progress bar to Moniter the progress of training
            progress_bar = tqdm(self.dataloader_train, desc='Epoch {:1d}'.format(epoch), leave=False, disable=False)

            for batch in progress_bar:
                torch.backends.cudnn.enabled = False

                model.zero_grad() # As we not working with thew RNN's
                
                # As our dataloader has '3' iteams so batches will be the Tuple of '3'
                #batch = tuple(b.to(device) for b in batch)
                
                batch = tuple(b for b in batch)

                # INPUTS
                inputs = {'input_ids':    batch[0],
                        'attention_mask': batch[1],
                        'labels':         batch[2],
                        }      

                # OUTPUTS
                outputs = model(**inputs) # '**' Unpacking the dictionary stright into the input
                
                loss = outputs[0]
                loss_train_total += loss
                loss.backward()           # backpropagation

                # Gradient Clipping -- Taking the Grad. & gives it a NORM value ~ 1 
                torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

                optimizer.step()
                scheduler.step()
                
                progress_bar.set_postfix({'training_loss': '{:.3f}'.format(loss.item()/len(batch))})
                
                
            torch.save(model.state_dict(), f'finetuned_WangchanBERTa_epoch_{epoch}.model')
                
            tqdm.write(f'\nEpoch {epoch}')
            
            loss_train_avg = loss_train_total/len(self.dataloader_train)            
            tqdm.write(f'Training loss: {loss_train_avg}')
            
            val_loss, predictions, true_vals = self.evaluate(self.dataloader_validation)
            val_f1 = self.f1_score_func(predictions, true_vals)
            tqdm.write(f'Validation loss: {val_loss}')
            tqdm.write(f'F1 Score (Weighted): {val_f1}')

    def evaluate_specific_model(self, evaluate_model_file_path):
        model = AutoModelForSequenceClassification.from_pretrained(self.MODEL_NAME,
                                                                num_labels=len(self.label_dict),
                                                                output_attentions=False,
                                                                output_hidden_states=False)

        model.load_state_dict(torch.load(evaluate_model_file_path, map_location=torch.device('cpu')))
        _, predictions, true_vals = self.evaluate(self.dataloader_validation)
        self.accuracy_per_class(predictions, true_vals)

    def pipeline(self):
        torch.backends.cudnn.enabled = False
        df = self.import_data()
        encoded_data_train, encoded_data_val = self.encode_data(df)
        self.data_loader(df , encoded_data_train, encoded_data_val)
        model, optimizer, scheduler, device = self.model_setting()
        self.training(model, optimizer, scheduler, device)

if __name__ == '__main__':
    torch.backends.cudnn.enabled = False
    tuning = fine_tune_wangchanberta()
    tuning.pipeline()
    for i in range(tuning.EPOCHS):
        tuning.evaluate_specific_model('finetuned_WangchanBERTa_epoch_{i}.model')
