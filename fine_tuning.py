import sys
import datasets
import torch
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split


from transformers import Trainer
from transformers import TrainingArguments
from transformers import AutoTokenizer, AutoModelForSequenceClassification, AutoConfig

class DataLoader(torch.utils.data.Dataset):
    def __init__(self, sentences=None, labels=None):
        self.sentences = sentences
        self.labels = labels
        self.DEVICE = torch.device('cpu')
        self.MODEL_NAME = "airesearch/wangchanberta-base-att-spm-uncased"
        self.tokenizer = AutoTokenizer.from_pretrained(self.MODEL_NAME)
        self.MAX_LENGTH = 256
        
        if bool(sentences):
            self.encodings = self.tokenizer(self.sentences,
                                            padding=True,
                                            truncation=True,
                                            max_length=self.MAX_LENGTH,
                                            return_tensors="pt")
        
    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        
        if self.labels == None:
            item['labels'] = None
        else:
            item['labels'] = torch.tensor(self.labels[idx])
        return item    
        
    def __len__(self):
        return len(self.sentences)
    
    def encode(self, x):
        return self.tokenizer(x, return_tensors = 'pt').to(self.DEVICE)


class fine_tune():
    def __init__(self):
        self.EPOCHS = int(sys.argv[1])
        self.BATCH_SIZE = int(sys.argv[2])
        self.FILE_PATH = 'wisesight.csv'
        self.MODEL_NAME = "airesearch/wangchanberta-base-att-spm-uncased"
        self.DEVICE = torch.device('cpu')
        self.f1 = datasets.load_metric('f1')
        self.accuracy = datasets.load_metric('accuracy')
        self.precision = datasets.load_metric('precision')
        self.recall = datasets.load_metric('recall')
        

    def import_data(self):
        self.df = pd.read_csv(self.FILE_PATH)

        self.le = LabelEncoder()
        self.df['label'] = self.le.fit_transform(self.df['category'])

        x_train, x_val, y_train, y_val =  train_test_split(self.df.index.values,
                                                        self.df.label.values,
                                                        test_size=0.10,
                                                        random_state=42,
                                                        stratify=self.df.label.values
        )

        self.df['data_type'] = ['not_set']*self.df.shape[0]
        self.df.loc[x_train, 'data_type'] = 'train'
        self.df.loc[x_val, 'data_type'] = 'val'

        return self.df

    def encode_data(self):
        self.train_dataset = DataLoader(self.df[self.df.data_type=='train'].texts.values.tolist(), self.df[self.df.data_type=='train'].label.values)
        self.val_dataset = DataLoader(self.df[self.df.data_type=='val'].texts.values.tolist(), self.df[self.df.data_type=='val'].label.values)
    
    def compute_metrics(self, eval_pred):
        metrics_dict = {}
        predictions, labels = eval_pred
        predictions = np.argmax(predictions, axis=1)
        
        metrics_dict.update(self.f1.compute(predictions = predictions, references = labels, average = 'macro'))
        metrics_dict.update(self.accuracy.compute(predictions = predictions, references = labels))
        metrics_dict.update(self.precision.compute(predictions = predictions, references = labels, average = 'macro'))
        metrics_dict.update(self.recall.compute(predictions = predictions, references = labels, average = 'macro'))    
        
        return metrics_dict

    def config_model(self):
        id2label = {idx:label for idx, label in enumerate(self.le.classes_)}
        label2id = {label:idx for idx, label in enumerate(self.le.classes_)}

        config = AutoConfig.from_pretrained(self.MODEL_NAME,
                                            num_labels = 4,
                                            id2label = id2label,
                                            label2id = label2id)

        self.model = AutoModelForSequenceClassification.from_config(config)

    def train_model(self):

        training_args = TrainingArguments(output_dir='/wangchanberta/results',
                                        num_train_epochs=self.EPOCHS,
                                        per_device_train_batch_size=self.BATCH_SIZE,
                                        per_device_eval_batch_size=self.BATCH_SIZE,
                                        evaluation_strategy='steps',
                                        logging_steps=50,
                                        save_total_limit = 1,
                                        save_strategy = "no"
                                        )

        trainer = Trainer(model=self.model,
                        args=training_args,
                        train_dataset=self.train_dataset,
                        eval_dataset=self.val_dataset,
                        compute_metrics=self.compute_metrics)
        
        trainer.train()
        trainer.save_model('/wangchanberta')

    def pipeline(self):
        self.import_data()
        self.encode_data()
        self.config_model()
        self.train_model()
        
if __name__ == '__main__':
    tuning = fine_tune()
    tuning.pipeline()